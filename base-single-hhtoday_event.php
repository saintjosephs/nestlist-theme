<?php get_template_part('templates/head'); ?>
<body <?php body_class('hhtoday-theme'); ?>>
  
  <div class="hhtoday-wrapper">
    <?php
      get_template_part('templates/header-hhtoday');
    ?>
    <div class="hht-content clearfix">
      <main class="main" role="main">
        <h1 class="hht-single-header">Events</h1>
        <?php while (have_posts()) : the_post(); ?>
          <?php get_template_part('templates/content', 'advert'); ?>
        <?php endwhile; ?>
      </main><!-- /.main -->
      <aside role="complementary">
        <?php include roots_sidebar_path(); ?>
      </aside><!-- /.sidebar -->
    </div><!-- /.content -->
    <?php get_template_part('templates/adverts-footer'); ?>
  </div>

</body>
</html>
