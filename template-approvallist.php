<?php
/*
Template Name: Approval List
*/
?>

<?php get_template_part('templates/page', 'header'); ?>
<?php get_template_part('templates/content', 'page'); ?>

<div class="panel-group" id="accordion">

<?php
$args = array(
	'numberposts'	=> 100,
  'offset'		=> 0,
  'orderby'		=> 'title',
  'order'			=> 'ASC',
  'post_type'		=> 'post',
	'post_status'	=> array('draft','publish')
);


$myposts = get_posts($args);
foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a href="#entry<?php echo get_field( "entry_id" ) ?>" data-toggle="collapse" data-parent="#accordion"><?php echo get_field( "entry_id" ) ?><?php the_title(); ?></a>
    </h4>
  </div>
  <div class="panel-collapse collapse in" id="entry<?php echo get_field( "entry_id" ) ?>">
    <div class="panel-body">
    
    <strong>Submitted by</strong> [100] ([101])
    
    <strong>Submission Date</strong> [created-at]
    
    <strong>Category</strong> [103]
    
    <strong>Website</strong> [162]
    
    <strong><em>Posting Content</em></strong>
    <div class="table-callout table-callout-info">
      <div class="media">
        <a class="pull-left" href="#"><img class="media-object" alt="" src="[160]" /></a>
        <div class="media-body">
          <h4 class="media-heading"><?php the_title(); ?></h4>
          <?php get_template_part('templates/content', 'listing'); ?>
        </div>
      </div>
    </div>
    <?php echo FrmAppController::get_form_shortcode(array('id' => 8, 'key' => '', 'title' => false, 'description' => false, 'readonly' => false, 'entry_id' => get_field( "entry_id" ), 'fields' => '167,168,178')); ?>
    </div>
  </div>
</div>
<?php endforeach; 
wp_reset_postdata();
          
?>

</div>