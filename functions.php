<?php
/**
 * Roots includes
 */
require_once locate_template('/lib/utils.php');           // Utility functions
require_once locate_template('/lib/init.php');            // Initial theme setup and constants
require_once locate_template('/lib/wrapper.php');         // Theme wrapper class
require_once locate_template('/lib/sidebar.php');         // Sidebar class
require_once locate_template('/lib/config.php');          // Configuration
require_once locate_template('/lib/activation.php');      // Theme activation
require_once locate_template('/lib/titles.php');          // Page titles
require_once locate_template('/lib/cleanup.php');         // Cleanup
require_once locate_template('/lib/nav.php');             // Custom nav modifications
require_once locate_template('/lib/gallery.php');         // Custom [gallery] modifications
require_once locate_template('/lib/comments.php');        // Custom comments modifications
require_once locate_template('/lib/relative-urls.php');   // Root relative URLs
require_once locate_template('/lib/widgets.php');         // Sidebars and widgets
require_once locate_template('/lib/scripts.php');         // Scripts and stylesheets
require_once locate_template('/lib/custom.php');          // Custom functions
require_once locate_template('/lib/hhtoday.php');          // Custom functions




add_action('save_post','nestlist_update_data');
  
remove_filter('the_title', 'wptexturize');
  
function nestlist_update_data($post_id){
    global $post, $wpdb, $date;
    
    $date = date('l jS \of F Y h:i:s A');
    if (wp_is_post_autosave($post_id) || get_post_status($post_id) == 'auto-draft'){
      return; // if post is an autosave or an autodraft, return
    }
    
    $cache_file = get_stylesheet_directory().'/_source/data.js';
    
    ob_start();
    echo "/* Data last updated: ".$date." */\n";
    echo "var sjunestlistposts = ";
    
    echo "["; // start the object
    // WP_Query arguments
    $args = array (
    	'order'                  => 'DESC',
    	'orderby'                => 'date',
    	'posts_per_page'         => 100,
    	'post_status'     => 'publish'
    );
    
    // The Query
    $query = new WP_Query( $args );
    
    // The Loop
    if ( $query->have_posts() ) {
    	while ( $query->have_posts() ) {
    		$query->the_post();
    		// each single object
    		$thepostid = get_the_ID();
    		//echo 'location:';
    		echo '{';
    		
    		echo '"ID":'.$thepostid.',';
    		$nocurlytitle = html_entity_decode(str_replace("& #8217","'",get_the_title()));
    		echo '"title":"'.$nocurlytitle.'",';
    		echo '"slug":"'.get_permalink($thepostid).'",';
    		echo '"post-status":"'.get_post_status($thepostid).'",';
    		
    		// get the content
    		$the_content = '';
    		$the_content=json_encode(get_the_content());
    		echo '"content":'.$the_content.',';
    		
    		// get the slug of ONLY the first category
    		$n = 1;
    		$the_cat_slug = '';
    		foreach(get_the_category() as $category) {
          $the_cat_slug = $category->slug;
          if($n == 1) break;
        }
    		echo '"category":"'.$the_cat_slug.'",';
    		
    		// get the URL for the thumbnail sized featured image, then the big sized featured image
    		$thumb_id = ''; $thumb_url_array = '';
    		$thumb_id = get_post_thumbnail_id();
        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
    		echo '"featuredthumb":"'.$thumb_url_array[0].'",';
    		
    		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
    		echo '"featuredbig":"'.$thumb_url_array[0].'",';
    		
    		// is there a website?  let's get it!
    		if(get_field('website')){
      		echo '"website":"'.get_field('website').'",';
    		} else {
      		echo '"website":"",';
    		}
    		if(get_field('contact_name')){
      		echo '"contactname":"'.get_field('contact_name').'",';
    		} else {
      		echo '"contactname":"'.get_the_author().'",';
    		}
    		if(get_field('contact_email')){
      		echo '"contactemail":"'.get_field('contact_email').'",';
    		} else {
      		echo '"contactemail":"'.get_the_author_meta( 'user_login' ).'",';
    		}
    		if(get_field('post_begin_date')){
      		echo '"date":"'.date('F d, Y', strtotime(get_field('post_begin_date'))).'",';
    		} else {
      		echo '"date":"'.get_the_date().'",';
    		}
    		
    		
    		      
    		echo "},\n";

    	}
    } else {
    	// no posts found
    }

    
    // Restore original Post Data
    wp_reset_postdata();


    
    echo "];";
    $output = ob_get_contents(); 
    file_put_contents($cache_file, $output);
    //ob_end_flush();
    ob_get_clean();
    return;
  }



// this will cause all of the HHToday Posts to be deleted
// before we run the import.
add_action('pmxi_before_xml_import', 'hhtoday_before_xml_import', 10, 1);

function hhtoday_before_xml_import($import_id) {
  if($import_id == 5){
    // Get 50 custom post types pages, set the number higher if is not slow.
    $mycustomposts = get_posts( array( 'post_type' => 'hhtoday', 'posts_per_page' => 50) );
    foreach( $mycustomposts as $mypost ) {
      wp_delete_post( $mypost->ID, true); // Set to False if you want to send them to Trash.
    }
  }
}

