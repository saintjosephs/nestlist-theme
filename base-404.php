<?php get_template_part('templates/head'); ?>
<body <?php body_class('hhtoday-theme'); ?>>
  
  <div class="hhtoday-wrapper">
    <?php
      get_template_part('templates/header-hhtoday');
    ?>
    <div class="hht-content clearfix">
      <main class="main" role="main">
        <h1>Archived Posts</h1>
        <?php // WP_Query arguments
        $args = array (
        	'post_type'              => array( 'advert' ),
        	'post_status'            => array( 'expired' ),
          'orderby' => 'date',
          'order' => 'DESC',
      		'posts_per_page' => -1,
          // Using the date_query to filter posts from last month
          'date_query' => array(
              array(
                  'after' => '1 month ago'
              )
          )
        );
        
        // The Query
        $the_query = new WP_Query( $args );
        ?>
        
        <?php if ( $the_query->have_posts() ) : ?>
        <div class="hhtoday-archive-posts-notice">
        <p>You have reached this listing because you clicked on a link of an expired Hawk Hill Today announcement or event.</p>
        <p>Hawk Hill Today <strong>announcements</strong> run for only 3 days; an archived version is available for 30 days.</p>
        <p>Archived <strong>events</strong> are available </p>
        </div>
      	<!-- pagination here -->
        <ul class="hhtoday-archive-posts-list">
      	<!-- the loop -->
      	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
      		<li><a href="<?php the_permalink(); ?>"><span class="the-date"><?php echo get_the_date(); ?></span> <?php the_title(); ?></a></li>
      	<?php endwhile; ?>
      	<!-- end of the loop -->
        </ul>
      
      	<!-- pagination here -->
      
      	<?php wp_reset_postdata(); ?>
      
        <?php endif; ?>        
        
      </main><!-- /.main -->
      <aside role="complementary">
        <?php include roots_sidebar_path(); ?>
      </aside><!-- /.sidebar -->
    </div><!-- /.content -->
    <?php get_template_part('templates/adverts-footer'); ?>
  </div>

</body>
</html>