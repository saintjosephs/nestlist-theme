<?php get_template_part('templates/head'); ?>
<body <?php body_class('hhtoday-theme'); ?>>
  
  <div class="hhtoday-wrapper">
    <?php
      get_template_part('templates/header-hhtoday');
    ?>
    <div class="hht-content clearfix">
      <main class="main" role="main">
        <?php
          $categoryid = 0;
          $categories = get_the_terms( get_the_ID() , 'hhtoday_categories' );
          if($categories) {
          	foreach( $categories as $category ) {
          		$categoryid = $category->term_id;
          	}
          }
          if($categoryid == 47){ ?>
            <h1 class="hht-single-header">In Memoriam</h1>
          <?php } else { ?>
            <h1 class="hht-single-header">Announcement</h1>
          <?php }
        ?>
        <?php while (have_posts()) : the_post(); ?>
          <?php get_template_part('templates/content', 'advert'); ?>
        <?php endwhile; ?>
      </main><!-- /.main -->
      <aside role="complementary">
        <?php include roots_sidebar_path(); ?>
      </aside><!-- /.sidebar -->
    </div><!-- /.content -->
    <?php get_template_part('templates/adverts-footer'); ?>
  </div>

</body>
</html>
