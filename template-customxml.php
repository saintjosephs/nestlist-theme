<?php
/*
Template Name: Custom Feed for Classifieds
*/

$numposts = 100;

function yoast_rss_date( $timestamp = null ) {
  $timestamp = ($timestamp==null) ? time() : $timestamp;
  echo date(DATE_RSS, $timestamp);
}
function get_excerpt_by_id($post_id){
    $the_post = get_post($post_id); //Gets post ID
    $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
    $excerpt_length = 50; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if(count($words) > $excerpt_length) :
        array_pop($words);
        array_push($words, '…');
        $the_excerpt = implode(' ', $words);
    endif;

    $the_excerpt = '<p>' . $the_excerpt . '</p>';

    return $the_excerpt;
}
function yoast_rss_text_limit($string, $length, $replacer = '...') { 
  $string = strip_tags($string);
  if(strlen($string) > $length) 
    return (preg_match('/^(.*)\W.*$/', substr($string, 0, $length+1), $matches) ? $matches[1] : substr($string, 0, $length)) . $replacer;   
  return $string; 
}


$posts = get_posts(array('post_type' => 'advert','posts_per_page' => 100));
$lastpost = $numposts - 1;

header("Content-Type: application/rss+xml; charset=UTF-8");
echo '<?xml version="1.0"?>';
?><rss version="2.0">
<channel>
  <title>Hawk Hill Today Feed for Classifieds</title>
  <link>https://nestlist.sju.edu/</link>
  <description>Classifieds from Hawk Hill Today for feeding the Hawk Hill Today posts</description>
  <language>en-us</language>
  <pubDate><?php yoast_rss_date( strtotime('today GMT') ); ?></pubDate>
  <lastBuildDate><?php yoast_rss_date( strtotime($ps[$lastpost]->post_date_gmt) ); ?></lastBuildDate>
  <managingEditor>kpipe@sju.edu</managingEditor>

<?php foreach ($posts as $post) { ?>
  <item>
    <title><?php echo get_the_title($post->ID); ?></title>
    <link><?php echo get_permalink($post->ID); ?></link>
    
    <?php
      $file = "";
      $file_link = ''; 
      if( get_field('file', $post->ID) ): 
        $file  = get_field('file', $post->ID);
        $file_url = wp_get_attachment_url($file);
        $file_link = '<a href="'.$file_url.'">Download File</a><br />';
      endif; ?>

    <?php
	$bodytext = get_excerpt_by_id($post->ID);
	// use instead of yoast_rss_text_limit($post->post_content, 500)
    ?>
    
    
    <description><?php echo '<![CDATA['.$file_link.''.$bodytext.'<br/><br/>Keep reading: <a href="'.get_permalink($post->ID).'">'.get_the_title($post->ID).'</a>'.']]>';  ?></description>
    <pubDate><?php yoast_rss_date( strtotime($post->post_date_gmt) ); ?></pubDate>
    <guid><?php echo get_permalink($post->ID); ?></guid>
    
    <!--the custom fields-->
    <Website><![CDATA[<?php if( get_field('website') ): ?><?php the_field('website'); ?><?php endif; ?>]]></Website>
    <Image><![CDATA[<?php if( get_field('image') ): ?><?php the_field('image'); ?><?php endif; ?>]]></Image>
    <FeaturedImage><![CDATA[<?php if( get_field('image') ): ?><?php echo wp_get_attachment_url(get_field('image')); ?><?php endif; ?>]]></FeaturedImage>
    <File><![CDATA[<?php if( get_field('file') ): ?><?php the_field('file'); ?><?php endif; ?>]]></File>
    <ContactName><![CDATA[<?php if( get_field('contact_name') ): ?><?php the_field('contact_name'); ?><?php endif; ?>]]></ContactName>
    <ContactEmail><![CDATA[<?php if( get_field('contact_email') ): ?><?php the_field('contact_email'); ?><?php endif; ?>]]></ContactEmail>
    <StartDate><![CDATA[<?php if( get_field('start_date') ): ?><?php the_field('start_date'); ?><?php endif; ?>]]></StartDate>
    <EndDate><![CDATA[<?php if( get_field('end_date') ): ?><?php the_field('end_date'); ?><?php endif; ?>]]></EndDate>
    <ClassifiedsID><![CDATA[<?php echo $post->ID; ?>]]></ClassifiedsID>
    <HHTodayTaxs><![CDATA[
    <?php 
    $terms = get_the_terms( $post->ID , 'hhtoday_categories' );
    if($terms) {
    	foreach( $terms as $term ) {
    		echo $term->name;
    	}
    }
    ?>
    ]]>
    </HHTodayTaxs>
  </item>
<?php } ?>
</channel>
</rss>