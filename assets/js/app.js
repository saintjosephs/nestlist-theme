(function($) {
  'use strict';
  $('#collapseCategory').on('show.bs.collapse', function() {
    $('a.accordion-toggle > i').removeClass('glyphicon-collapse-up').addClass('glyphicon-collapse-down');
  });
  $('#collapseCategory').on('hide.bs.collapse', function() {
    $('a.accordion-toggle > i').removeClass('glyphicon-collapse-down').addClass('glyphicon-collapse-up');
  });
  
  $('select[name="item_meta[182]"]').change(function(){
    if($(this).val() == '18'){
      $("#field_mre5v5").val('publish');
    }
  });


})(jQuery);