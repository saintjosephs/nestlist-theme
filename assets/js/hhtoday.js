(function($) {

  var $hht_start_field = $("#field_mb9gbd2");
  var $hht_end_field = $("#field_yn32mr");
  var $hhtim_start_field = $("#field_mb9gbd3");
  // make the fields for the datepickers unable
  // to be typed into.
  $("#field_mb9gbd2").attr('readOnly', 'true' );
  $("#field_yn32mr").attr('readOnly', 'true' );

  var theDaysAdd = 1;
  theDaysAdd = $('#field_t77ycr').val();
  var theDaysAddPlusTwo = theDaysAdd + 86400000*2;

  
  
  
  $("#field_mb9gbd2").datepicker({
    dateFormat: "mm/dd/yy",
    minDate: theDaysAdd,
    defaultDate: theDaysAdd,
    setDate: theDaysAdd,
    maxDate: +365,
    onSelect: function (date) {
      var selectedDate=new Date(date);
      var threeDaysDay=86400000*2;
      var endDate=new Date(selectedDate.getTime()+threeDaysDay);
      
      var endDateString = '';
      
      var endDateDayTwoDigit = ("0" + endDate.getDate()).slice(-2);
      var endDateMonthTwoDigit = ("0" + (endDate.getMonth() + 1)).slice(-2);

      
      endDateString += endDateMonthTwoDigit + '/'+endDateDayTwoDigit+'/'+endDate.getFullYear();
      
      $("#field_io2zvu").val(endDateString); // the hidden field
      // set the main datepicker based on this
      $("#field_yn32mr").datepicker();
      $("#field_yn32mr").datepicker("option","minDate",selectedDate);
      $("#field_yn32mr").datepicker("option","maxDate",endDate);
      $("#field_yn32mr").datepicker("option","defaultDate",endDate);
      $("#field_yn32mr").datepicker("option","setDate",endDate);
      $("#field_yn32mr").attr('value',endDateString);
      $("#field_yn32mr").val(endDateString);
    }
  });

  var today=new Date();
  
  today.setTime( today.getTime() + 86400000 );
  var threeDaysDay=86400000*2;
  var endDate=new Date(today.getTime()+threeDaysDay);

  $('#field_yn32mr').datepicker({
    minDate: today,
    defaultDate: endDate,
    maxDate: endDate,
    setDate: endDate,
    dateFormat: "mm/dd/yy"
  });
  //start date
  
  
  
  /// Similar functions for the in memoriam form
  // make the fields for the datepickers unable
  // to be typed into.
  $("#field_mb9gbd3").attr('readOnly', 'true' );
  // set up the datepicker
  // so that when it is filled out, the end date hidden field is set as well.
  $("#field_mb9gbd3").datepicker({
    minDate: today,
    minDate: today,
    minDate: today,
    dateFormat: "mm/dd/yy",
    onSelect: function (date) {
      
      var selectedDate=new Date(date);
      var endDate=new Date(selectedDate.getTime());
      
      var endDateString = '';
      
      var endDateDayTwoDigit = ("0" + endDate.getDate()).slice(-2);
      var endDateMonthTwoDigit = ("0" + (endDate.getMonth() + 1)).slice(-2);

      endDateString += endDateMonthTwoDigit + '/'+endDateDayTwoDigit+'/'+endDate.getFullYear();
      $("#field_fd3uji2").val(endDateString); // the hidden field
    }

  });
  
})(jQuery);