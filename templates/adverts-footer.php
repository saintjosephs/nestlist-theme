<footer class="hhtoday-footer">
  <p>Hawk Hill Today is a daily email of announcements and events for the SJU community.</p>
  <p>All content is submitted directly by readers.</p>
  
  <p>Submissions Guidelines | <a href="https://nestlist.sju.edu/" target="_blank">Submit a Calendar Event</a> | Submit an Announcement</p>

</footer>
<?php wp_footer(); ?>
