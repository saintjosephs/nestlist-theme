<article <?php post_class(); ?>>
  <h1 class="entry-title"><?php the_title(); ?></h1>
  <div class="entry-content clearfix">
    <div class="entry-details">
    <p><strong>Contact</strong> <?php the_field('contact_name'); ?> <?php if(get_field('contact_email')): ?>(<a href="mailto:<?php echo get_field('contact_email');?>"><?php the_field('contact_email'); ?></a>)<?php endif;?></p>
    
    <?php if(get_field('website')): ?><p><strong>Website</strong> <a href="<?php echo get_field('website'); ?>"><?php echo get_field('website'); ?></a></p><?php endif; ?>
    </div>
    <?php if(get_field('image')): ?>
      <figure>
      <?php
        $image = get_field('image');
        $size = 'medium'; // (thumbnail, medium, large, full or custom size)
        
        if( $image ):
        	echo wp_get_attachment_image( $image, $size );
        endif;
      ?>
      </figure>
    <?php endif; ?>
    <?php if(is_singular( 'advert' )): ?>
    <?php the_content(); ?>
    <?php elseif(is_singular( 'hhtoday_event' )): ?>
    <?php the_content(); ?>
    <?php else: ?>
    <?php 
      $my_postid = get_field('classifieds_id');//This is page id or post id
      $content_post = get_post($my_postid);
      $content = $content_post->post_content;
      $content = apply_filters('the_content', $content);
      $content = str_replace(']]>', ']]&gt;', $content);
      echo $content;
    ?>
    <?php endif; ?>
  </div>
  <div class="entry-footer">
  <?php if(get_field('file')): ?>
    <p class="hht-file">
    <?php
    $file = get_field('file');
    if( $file ) {
    $url = wp_get_attachment_url( $file );
    ?><a href="<?php echo $url; ?>" >Download File</a><?php
    }
    ?>
    </p>
  <?php endif; ?>
  </div>
</article>