<div class="page-header clearfix">
  <h1>
    <?php echo roots_title(); ?>
    
  </h1>
</div>
<?php if(is_page('my-submissions')): ?>
  <?php 
    if ( current_user_can( 'publish_posts' ) ):
      
    ?>
    <h3><a class="btn btn-info" href="<?php echo site_url(); ?>/posts-to-approve/">Posts Awaiting Approval</a></h3>
  <?php endif; ?>
<?php endif; ?>

<!-- this is managed in page-header.php ... if you're on the review posts page, and you're an approver, you can 
  get the link to the approve posts page -->
<?php if(is_page(8084) || is_page(5556) ): ?>
  <div>
    <?php if ( current_user_can( 'publish_posts' ) ): ?>
    <h3 style="display:inline-block"><a class="btn btn-info" href="<?php echo site_url(); ?>/announcements/manage/">Posts Awaiting Approval</a></h3>
    <?php endif; ?>
    <?php if ( current_user_can( 'hhtoday_create_memorium' ) ): ?>
    <h3 style="display:inline-block"><a class="btn btn-warning" href="<?php echo site_url(); ?>/announcements/new-in-memoriam/">Create an In Memoriam Post</a></h3>
    <?php endif; ?>
  </div>
  
<?php endif; ?>