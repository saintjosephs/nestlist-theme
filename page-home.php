<?php get_template_part('templates/page', 'header'); ?>
<?php get_template_part('templates/content', 'page'); ?>
<div class="panel-group" id="fullnestlist">   
<?php $cats = get_categories(); ?>
<?php foreach($cats as $cat){ ?>
     
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">
        <a data-toggle="collapse" data-parent="#fullnestlist" href="#<?php echo $cat->slug; ?>" style="display:block;"><span class="badge pull-right"><?php echo $cat->count; ?></span>
          <?php echo $cat->name; ?>
        </a>
      </h3>
    </div>
    <div id="<?php echo $cat->slug; ?>" class="panel-collapse collapse">
      <div class="panel-body">
        <ul class="categorylist">
          <?php
          $myposts = get_posts("cat=$cat->term_id&post_per_page=0");;
          foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
          	<li>
          		<h3><i class="glyphicon glyphicon-plus"></i><a data-toggle="collapse" data-target="#<?php echo $post->post_name; ?>"><?php the_title(); ?></a></h3>
          		<div class="collapse" id="<?php echo $post->post_name; ?>">
          		<?php get_template_part('templates/content', 'listing'); ?>
          		</div>
              </li>
          <?php endforeach; 
          wp_reset_postdata();?>
        </ul>
      </div>
    </div>
  </div>

  
<?php } ?></div>