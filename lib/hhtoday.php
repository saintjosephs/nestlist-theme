<?php


add_image_size( 'hhtoday-thumb', 150, 9999 ); // Unlimited Height Mode


// hhtoday_hide_field_from_non_approver
//    hides the approval fields from non-approvers.
//    this allows the fields to still be generated
//    but sets them to be hidden.
add_filter( 'frm_field_type', 'hhtoday_hide_field_from_non_approver', 10, 2);
function hhtoday_hide_field_from_non_approver($type, $field){
  if(in_array($field->id, array(299, 300, 337))){ //299 is the reject checkbox
      if(!current_user_can('hhtoday_approve_posts'))//if current user does not have this capability
      $type = 'hidden'; //hide the fields
    }
  return $type;
}

//************
// FORM VALIDATION FUNCTIONS

// hhtoday_fix_empty_date_fields
//    when the form is submitted, there is a small possibility
//    of the "Start" and "End" dates coming in empty
//    so, when you are validating the form entry (before the submission goes through
//    and the post is created), we will utilize the hidden date fields
//    to fill in the start and end dates
//    runs for both the HHT and the HHTIM form
add_filter('frm_validate_field_entry', 'hhtoday_fix_empty_date_fields', 10, 3);
add_filter('frm_validate_field_entry', 'validate_custom_file_size', 20, 3);


function hhtoday_fix_empty_date_fields($errors, $posted_field, $posted_value){
  if ( $posted_field->id == 323 ) {
    if ($posted_value == ''){
      $_POST['item_meta'][$posted_field->id] = $_POST['item_meta'][331];
    }
  }
  if ( $posted_field->id == 288 ) {
    if ($posted_value == ''){
      $_POST['item_meta'][$posted_field->id] = $_POST['item_meta'][333];
    }
  }

  // this is the same thing for the in memoriam form
  if ( $posted_field->id == 361 ) {
    if ($posted_value == ''){
      $_POST['item_meta'][$posted_field->id] = $_POST['item_meta'][375];
    }
  }

  return $errors;
}


function validate_custom_file_size($errors, $field, $value){
  // hhtim photo - 358
  // hhtim file - 359
  // hht photo - 295
  // hht file - 303
  if ($field->id == 303 and (isset($_FILES['file'.$field->id])) and !empty($_FILES['file'.$field->id]['name'])){
    if ((int)$_FILES['file'.$field->id]['size'] > 2000000){ // 1048576b = 10 mb
      $errors['field'.$field->id] = 'That file is too big. The maximum size for file uploads is 3Mb.  Please upload another.';
    }
  }
  if ($field->id == 359 and (isset($_FILES['file'.$field->id])) and !empty($_FILES['file'.$field->id]['name'])){
    if ((int)$_FILES['file'.$field->id]['size'] > 2000000){ // 1048576b = 10 mb
      $errors['field'.$field->id] = 'That file is too big. The maximum size for file uploads is 3Mb.  Please upload another.';
    }
  }
  return $errors;
}
// END FORM VALIDATION FUNCTIONS
//***********


//*************************************
// CUSTOM POST STATUS REALTED FUNCTIONS
// these need to run for both forms

// Custom Post Status are not showing up in the default "Status" field, so we have to add them
// both functions below handle this, the first one is for front-end editing and the second
// is for back-end editing
// 296 = post status field on HHT
// 368 = post status field on HHTIM
add_filter('frm_setup_new_fields_vars', 'hht_frm_add_custom_post_status', 20, 2);
function hht_frm_add_custom_post_status($values, $field){
  if($field->id == 296 || $field->id == 368){
    $values['options'][] = array(
      'label' => 'Scheduled',
      'value' => 'future'
    );
    $values['options'][] = array(
      'label' => 'Expired',
      'value' => 'expired'
    );
  }
  return $values;
}
add_filter('frm_setup_edit_fields_vars', 'hht_frm_backend_add_custom_post_status', 20, 3);
function hht_frm_backend_add_custom_post_status($values, $field, $entry_id){
  if($field->id == 296 || $field->id == 368){

    $values['options']['future'] = 'Scheduled';
    $values['options']['expired'] = 'Expired';

  }
  
  return $values;
}


// Ok, as an added bonus challenge, we need to hook into the javascript to add the
// status options to the Views editing page.  The Views are custom post types created by
// Formidable, so the $hook is actually a post.php, so only add this jquery
// in the admin on the posts.php page

function hhtoday_admin_views_load_scripts($hook) {
	if( $hook != 'post.php' && $hook != 'post-new.php' )
		return;

  wp_register_script('hhtodayadmin', get_template_directory_uri() . '/lib/hhtoday_admin.js', array('jquery'), true);
  wp_enqueue_script('hhtodayadmin');
}
add_action('admin_enqueue_scripts', 'hhtoday_admin_views_load_scripts');

// END CUSTOM POST STATUS REALTED FUNCTIONS
//*************************************


//******************************************
// IN MEMORIAM FORM PROCESSING FUNCTIONS



// In Memoriam Form
//    when an in memoriam is posted   frm_after_create_entry
//    or updated                      frm_after_update_entry
//    it is auto-approved.            hhtoday_auto_approve_in_memorium
//    an email will need to go to the submitter (handled in formidable)

/* hhtoday_auto_approve_in_memorium
    Our "In Memorium" listings should be auto-approved.
    They should also be set to only run for a single day.
*/

add_action('frm_after_create_entry', 'hhtoday_auto_approve_in_memorium', 30, 2);
add_action('frm_after_update_entry', 'hhtoday_auto_approve_in_memorium', 10, 2);
function hhtoday_auto_approve_in_memorium($entry_id, $form_id){
  $newTZ = new DateTimeZone("America/New_York");

   
  
  if($form_id === 15){
    if(!isset($_POST['item_meta'][368])){
      $_POST['item_meta'][368] = 'scheduled';
    }
    if(isset($_POST['item_meta'][368])){
      $posted_approval_status = $_POST['item_meta'][368];
      if($posted_approval_status === 'draft')
      $_POST['item_meta'][368] = 'publish';
    }
    
    $entry = FrmEntry::getOne( $entry_id );
    $post_id = $entry->post_id;
    $entry_id = $entry->id;
    
    // for in-memoriam, the post start and end dates are the same
    // so we only really need to read this from the "posting date" field.
    $post_date = $_POST['item_meta'][361];
    $final_post_date = new DateTime( $post_date, $newTZ );
    $final_post_date->setTime( 0, 0, 0 );
    
    // now that we have our start and end dates calculated
    // set the start and end dates on the form to those values
    // hhtoday_formidable_update_form_field($entry_id, 361, $final_post_date->format('Y-m-d'));
    // hhtoday_formidable_update_form_field($entry_id, 323, $final_post_date->format('Y-m-d'));

    // set the start and end dates on the post to those values (ACF Fields)
    // update_field('field_5655c6cbbd545', $final_post_date->format('Y-m-d'), $entry->post_id);
    // update_field('field_5655c6d8bd546', $final_post_date->format('Y-m-d'), $entry->post_id);

    // set the post date to the start date
    // from the codex - http://codex.wordpress.org/Function_Reference/wp_update_post#Scheduling_posts
    //   if you are trying to use wp_update_post() to schedule an existing draft, it will not work unless you pass
    //   $my_post->edit_date = true. WordPress will ignore the post_date when updating drafts unless edit_date is true.
    // Also pass 'post_date_gmt' so that WP plays nice with dates
    $gmDate = new DateTime($final_post_date->format('Y-m-d'), new DateTimeZone('GMT'));
    $post_to_update = array(
        'ID'           => $entry->post_id,
        'edit_date'    => true,
        'post_date'    => $final_post_date->format('Y-m-d H:i:s'),
        'post_date_gmt'=> $gmDate->format('Y-m-d H:i:s')
    );
    wp_update_post( $post_to_update );

    // set the expiry date to the end date
    $final_post_date->setTime( 12, 0, 0 );
    update_post_meta( $entry->post_id, "_expiration_date", $final_post_date->getTimestamp() );
    
    
    
  }
}


// END IN MEMORIAM FORM PROCESSING FUNCTIONS
//******************************************


//hhtoday_frm_set_dates_initial_values
//  sets up the initial values for the datetime and the hidden fields
//  in the form
//  runs for the HHT form
add_filter('frm_setup_new_fields_vars', 'hhtoday_frm_set_dates_initial_values', 20, 2);
function hhtoday_frm_set_dates_initial_values($values, $field){
  
  // set these values to the field IDs in the form
  $hht_start_selector=288;
  $hht_end_selector=323;
  $hht_start_calculator=333;
  $hht_end_calculator=331;
  $hhtoday_hidden_datepicker_calculator=335;

  if($field->id == $hht_start_selector || $field->id == $hht_end_selector || $field->id == $hht_start_calculator || $field->id == $hht_end_calculator){
    $rightnow = hhtoday_get_current_tz_date();
    $startdate = hhtoday_get_current_tz_date();
    $enddate = hhtoday_get_current_tz_date();

    $daystoaddstart = 1; $daystoaddend = 1;
    $daystoaddstart = hhtoday_get_initial_start_value($rightnow);
    $daystoaddend = $daystoaddstart + 2;

    $startdate->modify('+'.$daystoaddstart.' days');
    if($field->id == $hht_start_selector || $field->id == $hht_start_calculator){
      $values['value'] = $startdate->format('m/d/Y');
    }

    $enddate->modify('+'.$daystoaddend.' days');
    if($field->id == $hht_end_selector || $field->id == $hht_end_calculator){
      $values['value'] = $enddate->format('m/d/Y');
    }
  }


  if($field->id == $hhtoday_hidden_datepicker_calculator){
    $rightnow = hhtoday_get_current_tz_date();
    $values['value'] = hhtoday_get_initial_start_value($rightnow);
  }

  return $values;
}

//hhtodayIM_frm_set_dates_initial_values
//  sets up the initial values for the datetime and the hidden fields
//  in the form
//  runs for the HHT In Memoriam form
//  they can ALWAYS select tomorrow as a post date, even if it is past the expiry time
//  the start and end dates for HHTIM are the same, so there is no need to enddate calculate
add_filter('frm_setup_new_fields_vars', 'hhtodayIM_frm_set_dates_initial_values', 20, 2);
function hhtodayIM_frm_set_dates_initial_values($values, $field){
  
  // set these values to the field IDs in the form
  $hhtIM_start_selector=361;
  $hhtIM_start_calculator=375;

  if($field->id == $hhtIM_start_selector || $field->id == $hhtIM_start_calculator){
    $startdate = hhtoday_get_current_tz_date();
    
    $startdate->modify('+1 days');
    if($field->id == $hhtIM_start_selector || $field->id == $hhtIM_start_calculator){
      $values['value'] = $startdate->format('m/d/Y');
    }
  }

  return $values;
}



/* ================== */
/* NON-FORM FUNCTIONS */
/* ================== */

add_action( 'admin_init', 'hhtoday_set_default_rolecaps');
function hhtoday_set_default_rolecaps(){
	//add role capabilities
	//get the "administrator" role object
	$rolesadmin=array('administrator','super_admin');

	foreach($rolesadmin as $roladm){
		$role = get_role($roladm);
		if(!$role) continue;
		//add hhtoday's capabilities to it so that other widgets can reuse it
		$arr=array('hhtoday_create_memorium','hhtoday_approve_posts');
    
		foreach($arr as $arrkey){
			if(!$role->has_cap($arrkey)) $role->add_cap( $arrkey );
		}
	}
}
// ************
// function that removes HHT events 
add_action('remove_hht_events', 'remove_hht_events_function');
function remove_hht_events_function(){
  global $wpdb;

  $args = array(
    'posts_per_page'  => -1,
    'post_type'       => 'hhtoday_event',
  );
  $findHHToday = get_posts( $args );
  foreach($findHHToday as $post){
    $post_id = $post->ID;
    $attachment = get_post_thumbnail_id( $post_id );
    if($attachment){
      wp_delete_attachment( $attachment, true );
    }
    wp_delete_post( $post_id, true );
  }
}

// ************
// The following set of fuctions manage the subscription list
// since technically individual employees can unsubscribe from the email,
// causing the entire alias to be unsubbed
// as a matter of policy they should not unsub.  If we find that the employee
// alias is unsubbed, add it back
//
// creation of a cron schedule
// running of that cron schedule
// determining if employee is subscribed
//
// Ensure user is added to the WYSIJA list to stop employee from unsubscribing
add_action('check_if_employee_is_subscribed_event', 'check_if_employee_is_subscribed_function');
add_filter('cron_schedules','hhtoday_cron_schedules');
add_action('wp', 'check_if_employee_is_subscribed_activation');
function hhtoday_cron_schedules($schedules){
    if(!isset($schedules["5min"])){
        $schedules["5min"] = array(
            'interval' => 5*60,
            'display' => __('Once every 5 minutes'));
    }
    return $schedules;
}
function check_if_employee_is_subscribed_activation() {
	if ( !wp_next_scheduled( 'check_if_employee_is_subscribed_event' ) ) {
		wp_schedule_event( current_time( 'timestamp' ), '5min', 'check_if_employee_is_subscribed_event');
	}
}
function check_if_employee_is_subscribed_function() {
  global $wpdb;

  // employee user id = 13702
  // etest@sju.edu = 13650
  // ka test list id = 4
  // employee (real) list id = 5

   //$query =  "INSERT IGNORE INTO `wp_wysija_user_list`(`list_id`, `user_id`,`sub_date`,`unsub_date`) VALUES ('4','13650',' 1390336097','0')";

  // first, see if the user is subscribed
  $list_ids = array( 5 );
	$data_subscriber = array(
  	'user' => array(
  		'email' => 'employee@sju.edu'
  	),
  	'user_list' => array(
  		'list_ids' => $list_ids
  	),
  );

  $user_id = WYSIJA::get( 'user', 'helper' )->addSubscriber( $data_subscriber );

  // then, add it to the list
  $query =  "UPDATE wp_wysija_user_list SET unsub_date='0' WHERE user_id='13702' AND list_id='5'";
	if( $wpdb->query($query) ) { $requestResponse = ' done'; } else { $requestResponse = ' error'; }
}

// end set of functions for managing employee unsubscribes
//*****************


// Let's add a dashboard widget for our super users to give them quick ways to duplicate emails
add_action( 'wp_dashboard_setup', 'register_my_dashboard_widget' );
function register_my_dashboard_widget() {
 	global $wp_meta_boxes;

	wp_add_dashboard_widget(
		'hhtoday_officialcomm_widget',
		'Official Communications Mailings',
		'hhtoday_officialcomm_widget_display'
	);

 	$dashboard = $wp_meta_boxes['dashboard']['normal']['core'];

	$my_widget = array( 'hhtoday_officialcomm_widget' => $dashboard['hhtoday_officialcomm_widget'] );
 	unset( $dashboard['hhtoday_officialcomm_widget'] );

 	$sorted_dashboard = array_merge( $my_widget, $dashboard );
 	$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
}



/* ================ */
/* HELPER FUNCTIONS */
/* ================ */

//  is_hht_form($form_id)
//    returns true if the form_id is 15
function is_hht_form($form_id){
  if($form_id === 14){
    return true;
  }
  return false;
}


//  is_hht_im_form($form_id)
//    returns true if the form_id is 15
function is_hht_im_form($form_id){
  if($form_id === 15){
    return true;
  }
  return false;
}



// hhtoday_is_weekend
//  will return TRUE if it is Friday after the approval time, Saturday, or Sunday
//  takes a date object and does not modify it
function hhtoday_is_weekend($d){
  $day = $d->format('w');
  if( $day == 0 || $day == 6 ){ // day of the week, 0=sunday, 5=friday )
    return true;
  } else if ($day == 5 && hhtoday_is_past_approval_time( $d )) {
    return true;
  } else {
    return false;
  }
}

// hhtoday_is_past_approval_time
//  will return TRUE if it is after approval time.
//  takes a date object and does not modify it
function hhtoday_is_past_approval_time($d){
  $approval_time = 15; // set approval time to 3:00pm (h = 15)
  $hour = $d->format('H');
  if($hour >= 15){
    return true;
  } else {
    return false;
  }
}

// hhtoday_weekend_add_days($d)
//   takes a date object, and determines how many
//   days we will be adding to the start date
//   of the posting, based on the weekend
function hhtoday_weekend_add_days($d){
  $day = $d->format('w');

  // if it's not a weekend (or past approval time on F), return 0
  if(!hhtoday_is_weekend($d)){ return 0; }

  if ($day == 0){ return 2; }
  if ($day == 6){ return 3; }
  if ($day == 5){ return 4; }

  // just a default 0.
  return 0;
}

// hhtoday_get_current_tz_date
//  creates a DateTime object of right now, puts it in the correct time zone, and returns it.
function hhtoday_get_current_tz_date(){
  $d = new DateTime('now');
  $newTZ = new DateTimeZone("America/New_York");
  $d->setTimezone( $newTZ );
  return $d;
}

// hhtoday_get_initial_start_value
//  creates a DateTime object of right now, puts it in the correct time zone, and returns it.
function hhtoday_get_initial_start_value($d){
  $r = 1;

  if(hhtoday_is_weekend($d)){
    $r = hhtoday_weekend_add_days($d);
  } else if(hhtoday_is_past_approval_time($d)) {
    $r = 2;
  }

  return $r;
}

// hhtoday_make_tomorrow_no_time
// helper function, takes a date, 0:0:0 the time, adds a day
//    modifies the date object
function hhtoday_make_tomorrow_no_time(){
  $d = hhtoday_get_current_tz_date();
  $d->setTime( 0, 0, 0 );
  $d->modify('+1 day');
  return $d;
}



function hhtoday_formidable_update_form_field($entry_id, $field_id, $value){
  $updated = FrmEntryMeta::update_entry_meta( $entry_id, $field_id, '', $value );
  if ( ! $updated ) {
    FrmEntryMeta::add_entry_meta( $entry_id, $field_id, null, $value );
  }
}






/* hhtoday_update_dates_after_field_update
     HHToday Post Dating
        when the post is approved using the front-end interface,
        set the dates for the post.
        a post should always be scheduled based on when it is approved.
        if it is approved, it will be posted "tomorrow", unless it is scheduled by the submitter for a future date
        once it is approved, it will be posted for three days.
        utilizes the _expiration_date field from WPAdverts
*/
add_action( 'frm_after_update_field', 'hhtoday_update_dates_after_field_updates');
function hhtoday_update_dates_after_field_updates($atts){
  $newTZ = new DateTimeZone("America/New_York");

  if($atts['field_id']==296){ // we should only run this for when we are updating the Approval Status field for HHToday
      $entry = FrmEntry::getOne($atts['entry_id']);

      $post_id = $entry->post_id;
      $entry_id = $entry->id;

      if($atts['value']=='publish'){
        // if we are publishing the post, then we need to send the approval email
        // here we want to set the checkbox for "Approve" to Yes, so it can be used to send the email with the optional approval notice.
        hhtoday_formidable_update_form_field($entry_id, 342, 'yes');
        // trigger the email send!
        $form_id = 14;
        FrmFormActionsController::trigger_actions("update", $form_id, $entry_id, "email");
      }
      if($atts['value']=='expire'){
        // if the user chooses to expire their post, than we need to avoid sending the approval email.
        // since the checkbox for approve will still be set to yes, we need to have another checkbox
        // that can be used as an additional "and" for the email triggers.
        hhtoday_formidable_update_form_field($entry_id, 343, 'yes');
      }

      // create date objects for today and tomorrow
      $today = hhtoday_get_current_tz_date(); $today->setTime( 0, 0, 0 );
      $tomorrow = hhtoday_make_tomorrow_no_time();

      // get the values for the dates on the current entry
      $postedstartdate = FrmProEntriesController::get_field_value_shortcode(array('field_id' => 288, 'entry_id' => $entry_id, 'show'=>'value'));
      $postedenddate = FrmProEntriesController::get_field_value_shortcode(array('field_id' => 323, 'entry_id' => $entry_id, 'show'=>'value'));

      // created date objects based on the original posted dates
      $postedstartdateOB = new DateTime( $postedstartdate, $newTZ );
        $postedstartdateOB->setTime( 0, 0, 0 );
      //sometimes, the posted end date is empty!
      if($postedenddate == ''){
        $postedenddateOB = new DateTime( $postedstartdateOB->format('Y-m-d'), $newTZ );
          $postedenddateOB->modify('+3 days');
          $postedenddateOB->setTime( 0, 0, 0 );
      } else {
        $postedenddateOB = new DateTime( $postedenddate, $newTZ );
          $postedenddateOB->setTime( 0, 0, 0 );
      }



      // created date objects to handle the final start and end dates that we will pass to the values at the end
      $finalstartdate = new DateTime( $postedstartdate, $newTZ );
        $finalstartdate->setTime( 0, 0, 0 );
      // use the posted end date OB here because that was set correctly if the posted end date came in null
      $finalenddate = new DateTime( $postedenddateOB->format('Y-m-d'), $newTZ );
        $finalenddate->setTime( 0, 0, 0 );


      // First let's handle the end dates
      // we will compare the posted end date to this.  if it is GREATER than this, it should be set to this.
      // otherwise, leave it alone!
      $postedstartplusthreeOB = new DateTime( $postedstartdateOB->format('Y-m-d'), $newTZ );
        $postedstartplusthreeOB->setTime( 0, 0, 0 );
        $postedstartplusthreeOB->modify('+3 days');


      if($postedenddateOB > $postedstartplusthreeOB){
        $finalenddate = new DateTime( $postedstartplusthreeOB->format('Y-m-d'), $newTZ );
      }


      // Now let's handle the start dates
      //  we don't need to handle the case of $startdate being empty, since casting an empty string to a datetime sets it to today by default
      //    1) If the start date is before tomorrow, set it to tomorrow (we always want a posting to start after we approve it)
      //    2) If the start date is after tomorrow, leave it alone (this will allow for people to set future dates)
      if ($postedstartdateOB <= $tomorrow ){
        $finalstartdate = new DateTime( $tomorrow->format('Y-m-d'), $newTZ );
      }


      // and now that we've done all that work, see if it's an in memorium post
      //  and if it is, set the expire date to one more than the post date.
      $categoryid = 0;
      $categories = get_the_terms( $post_id , 'hhtoday_categories' );
      if($categories) {
      	foreach( $categories as $category ) {
      		$categoryid = $category->term_id;
      	}
      } else {
        wp_set_object_terms( $post_id, array( 46 ), 'hhtoday_categories' );
        hhtoday_formidable_update_form_field($entry_id, 286, 46);
      }
      // if the thing is an in-memorium (category 47), the final end date should be the posted start date (only runs for one day)
      if($categoryid == 47){
        $finalenddate = new DateTime( $postedstartdate, $newTZ );
      }

      // now that we have our start and end dates calculated
      // set the start and end dates on the form to those values
      hhtoday_formidable_update_form_field($entry_id, 288, $finalstartdate->format('Y-m-d'));
      hhtoday_formidable_update_form_field($entry_id, 323, $finalenddate->format('Y-m-d'));

      // set the start and end dates on the post to those values (ACF Fields)
      update_field('field_5655c6cbbd545', $finalstartdate->format('Y-m-d'), $entry->post_id);
      update_field('field_5655c6d8bd546', $finalenddate->format('Y-m-d'), $entry->post_id);

      // set the post date to the start date
      // from the codex - http://codex.wordpress.org/Function_Reference/wp_update_post#Scheduling_posts
      //   if you are trying to use wp_update_post() to schedule an existing draft, it will not work unless you pass
      //   $my_post->edit_date = true. WordPress will ignore the post_date when updating drafts unless edit_date is true.
      // Also pass 'post_date_gmt' so that WP plays nice with dates
      $gmDate = new DateTime($finalstartdate->format('Y-m-d'), new DateTimeZone('GMT'));
      $post_to_update = array(
          'ID'           => $entry->post_id,
          'edit_date'    => true,
          'post_date'    => $finalstartdate->format('Y-m-d H:i:s'),
          'post_date_gmt'=> $gmDate->format('Y-m-d H:i:s')
      );
      wp_update_post( $post_to_update );

      // set the expiry date to the end date
      $finalenddate->setTime( 12, 0, 0 );
      update_post_meta( $entry->post_id, "_expiration_date", $finalenddate->getTimestamp() );

    }
}