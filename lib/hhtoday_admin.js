jQuery(document).ready(function($){
  $('#frm_where_options .frm_where_row').each(function(){ 
    var theValSelected = $(this).find('#where_field_id option:selected').val();
    // this is the id of the "Post Status" field 
    //  in both the HHT and the HHTIM form
    if(theValSelected == '296' || theValSelected == '368'){
      var selectValues = $(this).find('.frm_where_val select');
      selectValues.find('option').each(function(){
        $(this).text($(this).attr('value'));
     });
     selectValues.append($('<option></option>').val('future').html('Scheduled'));
     selectValues.append($('<option></option>').val('expired').html('Expired'));
    }
   });
});
