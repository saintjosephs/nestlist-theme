<?php
/**
 * Custom functions
 */

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
  if (!current_user_can('administrator') && !is_admin()) {
    show_admin_bar(false);
  }
}
 
 
// Set the SSL verification 
// To help run cron
// http://wordpress.stackexchange.com/questions/77981/upgrade-network-error-stuck-with-ssl-certificate-verification?rq=1
// http://wordpress.stackexchange.com/questions/115279/multisite-database-upgrade-ssl-error

add_filter(
        'http_request_args',
        'my_http_request_args',
        10,
        2
    );
 
function my_http_request_args($args, $url) {
    $args['sslverify'] = false;
    return $args;
}


 
 

// After an entry is created, link the entry ID to the post
// Store the entry ID in the field created by ACF
// This will help us to keep track of entries
add_action('frm_after_create_entry', 'after_entry_created', 41, 2);
function after_entry_created($entry_id, $form_id){
  if($form_id == 8){ //change 5 to the ID of your form
    global $frm_entry;
    $entry = $frm_entry->getOne($entry_id);
    update_field('field_52fbe6c45472a', $entry_id, $entry->post_id); // depends on ACF
  }
}

add_action('frm_after_update_field', 'frm_trigger_entry_update');
function frm_trigger_entry_update($atts){
  if($atts['field_id']==167){ // we should only run this for when we are updating the Approval Status field for NestList
    $entry = FrmEntry::getOne($atts['entry_id']);
    $current_user = wp_get_current_user();
    $the_approver = $current_user->display_name." (".$current_user->user_email.")";
    update_field('field_52fbe69e54727', 1, $entry->post_id); // depends on ACF // approved?
    update_field('field_52fbe6af54728', date('F j, Y h:i A'), $entry->post_id); // approval date
    update_field('field_52fbe6bd54729', $the_approver, $entry->post_id); // approver
    
    FrmProEntriesController::entry_update_field(array('id' => $entry->post_id, 'field_id' => '178', 'value' => $the_approver));
    // also expire posts.  this will help to auto-expire old posts
    expire_posts();
  }
}


// Function to expire posts.
// Runs as part of the approval process for entries
function expire_posts()
{
  global $wpdb;
    $daystogo = "30";

    $post_ids = $wpdb->get_results( "
        SELECT ID 
        FROM {$wpdb->posts}
        WHERE post_type ='post' 
        AND post_status = 'publish' 
        AND DATEDIFF(NOW(), post_date) > '{$daystogo}'
    " );
    foreach( $post_ids as $id )
    {
        $postid =  $id->ID;

        $my_post = array();
        $my_post['ID'] = $postid;
        $my_post['post_status'] = 'archive';
        wp_update_post( $my_post );
    }
    $post_ids = $wpdb->get_results( "
        SELECT ID 
        FROM {$wpdb->posts}
        WHERE post_type ='post' 
        AND post_status = 'publish'
    " );
    foreach( $post_ids as $id )
    {
       $postid =  $id->ID;
       $expiration_value = get_post_meta( $postid, 'post_end_date', true );

       if( $expiration_value )
       {
           $todays_date = date( "Y-m-d" );
           $today = strtotime( $todays_date );
           $expiration_date = strtotime( $expiration_value );
           if ( $expiration_date > $today )
           { 

           }
           else
           { 
               $my_post = array();
               $my_post['ID'] = $postid;
               $my_post['post_status'] = 'archive';

               wp_update_post( $my_post );
           }
        }
     }
    
}  



//  ****************************************
//  Rename the default Wordpress User Roles
//  And allow the author (our new "Approver") to edit others posts
//  ****************************************

function change_role_name() {
    global $wp_roles;

    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    //You can list all currently available roles like this...
    //$roles = $wp_roles->get_names();
    //print_r($roles);

    //You can replace "administrator" with any other role "editor", "author", "contributor" or "subscriber"...
    $wp_roles->roles['editor']['name'] = 'Special Announcement Poster';
    $wp_roles->role_names['editor'] = 'Special Announcement Poster';
    $wp_roles->roles['author']['name'] = 'Approver';
    $wp_roles->role_names['author'] = 'Approver';       
}
add_action('init', 'change_role_name');
function add_theme_caps() {
  // gets the author role
  $role = get_role( 'author' );

  // This only works, because it accesses the class instance.
  // would allow the author to edit others' posts for current theme only
  $role->add_cap( 'edit_others_posts' ); 
}
add_action( 'admin_init', 'add_theme_caps');

function auto_publish_specials(){
  // this is not actually a thing.  This happens via javascript
  // set in app.js
}




//  ****************************************
//  Creating an Archive status
//  ****************************************
function nestlist_custom_post_status(){
  register_post_status( 'archive', array(
    'label'                     => _x( 'Archive', 'post' ),
    'public'                    => false,
		'exclude_from_search'       => true,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
    'label_count'               => _n_noop( 'Archive <span class="count">(%s)</span>', 'Archive <span class="count">(%s)</span>' )
  ));
  register_post_status( 'rejected', array(
    'label'                     => _x( 'Rejected', 'post' ),
    'public'                    => false,
		'exclude_from_search'       => true,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
    'label_count'               => _n_noop( 'Rejected <span class="count">(%s)</span>', 'Rejected <span class="count">(%s)</span>' )
  ));
}
function nestlist_append_post_status_list(){
  global $post;
  $complete = '';
  $label = '';
  if($post->post_type == 'post'){
    if($post->post_status == 'archive'){
     $complete = ' selected=\"selected\"';
     $label = '<span id=\"post-status-display\"> Archived</span>';
    }
    if($post->post_status == 'rejected'){
     $rej_complete = ' selected=\"selected\"';
     $rej_label = '<span id=\"post-status-display\"> Rejected</span>';
    }
    echo '
    <script>
    jQuery(document).ready(function($){
     $("select#post_status").append("<option value=\"archive\" '.$complete.'>Archive</option>");
     $(".misc-pub-section label").append("'.$label.'");
     $("select#post_status").append("<option value=\"rejected\" '.$rej_complete.'>Rejected</option>");
     $(".misc-pub-section label").append("'.$rej_label.'");
    });
    </script>
    ';
  }
}
function nestlist_display_archive_state( $states ) {
  global $post;
  $arg = get_query_var( 'post_status' );
  if($arg != 'archive'){
    if($post->post_status == 'archive'){
      return array('Archive');
    }
  }
  if($arg != 'rejected'){
    if($post->post_status == 'rejected'){
      return array('Rejected');
    }
  }
  return $states;
}
add_action( 'init', 'nestlist_custom_post_status' );
add_action('admin_footer-post.php', 'nestlist_append_post_status_list');
add_filter( 'display_post_states', 'nestlist_display_archive_state' );

// Also make sure our custom statuses are set up in the selector for the post status.
add_filter('frm_setup_new_fields_vars', 'frm_populate_posts', 20, 2);
add_filter('frm_setup_edit_fields_vars', 'frm_populate_posts', 20, 2); //use this function on edit too
function frm_populate_posts($values, $field){
  if($field->id == 167){ 
    $values['options']['archive'] = 'Archive'; 
    $values['options']['rejected'] = 'Rejected';
    
    $values['use_key'] = true; //this will set the field to save the post ID instead of post title
  }
  return $values;
}

//  ****************************************
//  Adding role to body class for targeting of css and js
//  ****************************************

add_filter('body_class', 'nestlist_body_classes');

function nestlist_body_classes($classes) {
  global $current_user;
  foreach( $current_user->roles as $role )
    $classes[] = ' role-' . $role;
  return $classes;
}

// Helper Class - allows to check the currently logged in user role, or optionally
// accepts an integer User ID
function nestlist_check_user_role( $role, $user_id = null ) {
  if ( is_numeric( $user_id ) ) $user = get_userdata( $user_id );
  else $user = wp_get_current_user();
  if ( empty( $user ) ) return false;
  return in_array( $role, (array) $user->roles );
}

//if you are editing a post and you are just a subscriber (can't approve), set to draft.
add_filter('frm_validate_field_entry', 'nestlist_status_validation', 8, 3);
function nestlist_status_validation($errors, $posted_field, $posted_value){
  $userid = get_userdata( $user_id );
  if($posted_field->id == 167){
    if(nestlist_check_user_role('subscriber',$userid)){ 
      $_POST['item_meta'][$posted_field->id] = $_POST['frm_wp_post'][$posted_field->id .'=post_status'] = 'draft';
    }
  }
  // SET REJECTION
  if($posted_field->id == 266){
    if($posted_value === 'Yes'){
      
      $_POST['item_meta']['167'] = $_POST['frm_wp_post']['167=post_status'] = 'rejected';
      
      $thereason = $_POST['item_meta']['267'];
      if($thereason == '') $thereason = 'No reason provided';
      $entry = FrmEntry::getOne($_POST['id']);
      $current_user = wp_get_current_user();
      $the_approver = $current_user->display_name." (".$current_user->user_email.")";
      
      update_field('field_52fbe69e54727', 0, $entry->post_id); // depends on ACF // approved?
      update_field('field_52fbe6af54728', date('F j, Y h:i A'), $entry->post_id); // approval date
      update_field('field_52fbe6bd54729', $the_approver, $entry->post_id); // approver
      update_field('field_531f34f35811c', 1, $entry->post_id); // depends on ACF // approved?
      update_field('field_531f35125811d', $thereason, $entry->post_id);
      FrmProEntriesController::entry_update_field(array('id' => $entry->post_id, 'field_id' => '178', 'value' => $the_approver));
    }
      
  }
  return $errors;
}

add_filter('frm_validate_field_entry', 'calculate_time', 11, 3);
function calculate_time($errors, $field, $value){
  if($field->id == 186){ //change 25 to the ID of the hidden or admin only field which will hold the calculation
    $start = (strtotime($_POST['item_meta'][184])); // ID of the first field
    $end = (strtotime($_POST['item_meta'][185])); // ID of the first field
    $totaldays = ($end - $start)  / (60 * 60 * 24);;   
    $value = $_POST['item_meta'][186] = $totaldays; //change 25 to the ID of the hidden or admin only field which will hold the calculation
    
    if($totaldays > 30){
      $errors['field185'] = 'The start and end date can be no more than 30 days apart.  Please adjust.';
    }
    
  }
  return $errors;
}


// Change the button label on the "Reject" page to "Submit" instead of "Edit"
add_filter('frm_submit_button', 'change_my_submit_button_label', 10, 2);
function change_my_submit_button_label($label, $form){
  $label = 'Submit';//Change this text to the new Submit button label
  return $label;
}
